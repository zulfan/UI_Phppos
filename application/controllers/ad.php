<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad extends CI_Controller {

	
	public function index()
	{
		$this->load->view('ad/index');
	}
	public function create() {
		$this->load->view('ad/create');
	}
	public function editpelanggan() {
		$this->load->view('ad/editpelanggan');
	}
	public function customer() {
		$this->load->view('ad/customer');
	}
	public function items() {
		$this->load->view('ad/items');
	}
	public function newitem() {
		$this->load->view('ad/newitem');
	}
	public function suppliers() {
		$this->load->view('ad/suppliers');
	}
	public function newsupplier() {
		$this->load->view('ad/newsupplier');
	}
	public function reports() {
		$this->load->view('ad/reports');
	}
	public function expense() {
		$this->load->view('ad/expense');
	}
	public function newexpense() {
		$this->load->view('ad/newexpense');
	}
	public function prin() {
		$this->load->view('ad/print');
	}
	public function editprofil() {
		$this->load->view('ad/editprofil');
	}
	public function viewall() {
		$this->load->view('ad/viewall');
	}
	public function viewsent() {
		$this->load->view('ad/viewsent');
	}
	public function sendmessage() {
		$this->load->view('ad/sendmessage');
	}
	public function sentmessage() {
		$this->load->view('ad/sentmessage');
	}
	public function receiving() {
		$this->load->view('ad/receiving');
	}
	public function sales() {
		$this->load->view('ad/sales');
	}
	public function excelimpor() {
		$this->load->view('ad/excelimpor');
	}
	public function kelolatags() {
		$this->load->view('ad/kelolatags');
	}
}
