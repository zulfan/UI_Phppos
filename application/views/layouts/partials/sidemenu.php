<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | Dashboard</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">

    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="<?= base_url('ad/index') ?>">
            <i class="fa fa-tachometer"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?= base_url('ad/customer') ?>">
            <i class="fa fa-user"></i>
            <span>Pelanggan</span>
          </a>
        </li>
        <li>
          <a href="<?= base_url('ad/items') ?>">
            <i class="fa fa-hdd-o"></i> <span>Barang</span> 
          </a>
        </li>
        <li class="treeview">
          <a href="<?= base_url('ad/suppliers') ?>">
            <i class="fa fa-download"></i>
            <span>Suppliers</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?= base_url('ad/reports') ?>">
            <i class="fa fa-industry"></i>
            <span>Laporan</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?= base_url('ad/receiving') ?>">
            <i class="fa fa-cloud-download"></i> <span>Menerima</span>
          </a>
        </li>
        <li class="treeview">
          <a href="<?= base_url('ad/sales') ?>">
            <i class="fa fa-shopping-cart"></i> <span>Penjualan</span>
          </a>
        </li>
        <li>
          <a href="<?= base_url('ad/expense') ?>">
            <i class="fa fa-dollar"></i> <span>Expenses</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-power-off"></i> <span>Keluar</span>
          </a>
        </li>
      </ul>
      <!-- Modal HTML -->
      <div id="modal" class="modal fade">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Masukkan Nama tag</h4>
                  </div>
                  <div class="modal-body">
                      <div class="form-group">
                          <div class="col-md-12">
                              <input type="text" class="form-control">
                          </div>
                      </div>
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-primary">Ok</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                  </div>
              </div>
          </div>
      </div>
  </head>
</html>