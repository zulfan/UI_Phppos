<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
  .box{
    height: 1290x;
    width: 100%;
    border-color: #ffffff;
  }
  .form-group{
    padding-top: 13px;
  }
  .box1{
    height: 1290x;
    border-color: #ffffff;
    background-color: #ffffff;
    border-radius: 5px;
    padding-top: 12px;
    padding-bottom: 25px; 
  }
  th{
    background-color: #f9fbfc;
  }
  .box2{
    height: 1290x;
    border-color: #ffffff;
    background-color: #ffffff;
    padding-top: 20px;
    border-radius: 5px;
  }
</style>
<div class="row">
<div class="col-md-8">
    <div class="box">
        <div class="box-body">
            <div class="col-md-12">
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <a href="<?= base_url('ad/newitem')?>"><button class="btn btn-primary" type="button"><i class="fa fa-pencil-square-o" style="color: #ffffff;"></i></a></button>
                                </span>
                                <input type='text' class="form-control" placeholder="Masukkan nama barang atau pindai barcode">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button" style="background-color: #e0ffd4;  border-color: #85ef60; color: #85ef60;"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;&nbsp;<span>Menerima</span></button>
                                        <button class="btn btn-primary" type="button"><span>Tampilkan grafik</span></button>
                                    </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="box1">
            <div class="box-body">
                <div class="container">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="padding-left: 20px; padding-right: 20px;">
                        . . .
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="#">Menerima dan pembelian pesanan ditangguhkan </a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Membuat pesanan pembelian</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Batch Penerima</a></li>
                        </ul>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <a href="<?= base_url('ad/newsupplier')?>"><button class="btn btn-primary" type="button"><i class="fa fa-plus" style="color: #ffffff;"></i></a></button>
                            </span>
                            <input type='text' class="form-control" placeholder="Nama jenis Supplier...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-8">
    <div class="box2">
        <div class="box-body">
            <div class="container">
                <div class="col-sm-7">         
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Nama barang</th>
                                <th>Biaya</th>
                                <th>Qty</th>
                                <th>Diskon %</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="cart_content_area">
                                <td colspan="6">
                                    <div class="text-center text-warning"> <h3>There are no items in the cart <span class="flatBluec"> [Receivings]</span></h3></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<?php endsection() ?>

<?php getview('layouts/layout') ?>