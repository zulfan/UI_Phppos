<?php section('css') ?>
<link href="test.css">
<?php endsection() ?>
<style>
  .a{
    position: fixed;
    top: 20px;
    right: 5px;
  }
  .search {
    padding:8px 15px;
    background:rgba(5, 5, 5, 0.1);
    border:0px solid #f2f6f9;
  }
  .table {
    border-collapse: collapse;
    width: 100%;
    }
  .th, .td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
    }
  .tr:hover{background-color:#f5f5f5}
  .button{
    background-color: blue;
  }
  .page-heading{
    background-color: white;
    margin-bottom: 5px;
    width: 100%;
    padding: 20px;
  }
</style>
<?php section('content') ?>
  <section>
    <div id='main-content'>
      <header class='page-heading'>
        <div>
          <div class="row">
            <div class="col-md-2">
              <form>
                <input class="search" type="text" placeholder="Cari Pelanggan" required>
              </form>
            </div>
            <div class="col-md-offset-6 col-md-2" align="right">
              <a href="<?= base_url('ad/create')?>" class="btn btn-larg btn-primary" >Pelanggan Baru</a>
            </div>
            
            <div class="dropdown col-md-2">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="padding-right : 23px; padding-left: 23px;">
                .  .  .
              </button>
              <ul class="dropdown-menu">
                <li><a href="<?= base_url('ad/excelimpor')?>">Excel Impor</a></li>
                <li><a href="#">Excel Expor</a></li>
                <li><a href="#myModal" data-toggle="modal">Pembersihan pelanggan lama</a></li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </div>
  </section>
  <section>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Pelanggan</h3>
          <span class="badge" style="background-color: #367fa9;">9</span>
      </div>
        <div class="box-body">
          <table class="table">
              <thead>
                <thead>
                  <tr>
                    <th><input type="checkbox" aria-label="..." style="border-top: 5px; border-left: 5px; border-right: 5px;"></th>
                    <th class="th">ID</th>
                    <th class="th">Name</th>
                    <th class="th">E-Mail</th>
                    <th class="th">Nomor Tlp.</th>
                    <th class="th"></th>
                    <th class="th"></th>
                    <th class="th"></th>
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox" aria-label="..."></td>
                    <td class="td">11</td>
                    <td class="td">kazi shamim</td>
                    <td class="td">kazishamimm@gmail.com</td>
                    <td class="td"></td>
                    <td class="td"><button class="btn btn-primary button" style="padding-right : 12px;">Pay</button></td>
                    <td class="td"><a href="<?= base_url('ad/editpelanggan')?>">edit</a></td>
                    <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35">
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox" aria-label="..."></td>
                    <td class="td">17</td>
                    <td class="td">Jon</td>
                    <td class="td"></td>
                    <td class="td"></td>
                    <td class="td"><button class="btn btn-primary button" style="padding-right : 12px;">Pay</button></td>
                    <td class="td"><a href="<?= base_url('ad/editpelanggan')?>">edit</a></td>
                    <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35">
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox" aria-label="..."></td>
                    <td class="td">6</td>
                    <td class="td"> Khanh Beo</td>
                    <td class="td">qajo@gmail.com</td>
                    <td class="td">+841-32-4207261</td>
                    <td class="td"><button class="btn btn-primary button" style="padding-right : 12px;">Pay</button></td>
                    <td class="td"><a href="<?= base_url('ad/editpelanggan')?>">edit</a></td>
                    <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35">
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox" aria-label="..."></td>
                    <td class="td">6</td>
                    <td class="td"> Khanh Beo</td>
                    <td class="td">qajo@gmail.com</td>
                    <td class="td">+841-32-4207261</td>
                    <td class="td"><button class="btn btn-primary button" style="padding-right : 12px;">Pay</button></td>
                    <td class="td"><a href="<?= base_url('ad/editpelanggan')?>">edit</a></td>
                    <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35">
                  </tr>
                </thead>
              </thead>
          </table>
        </div>
      </div>
    </section>
    <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Konfirmasi</h4>
          </div>
          <div class="modal-body">
            <p>Apakah Anda yakin Anda ingin membersihkan semua pelanggan dihapus ? ( Ini akan menghapus nomor rekening dari pelanggan dihapus sehingga mereka dapat digunakan kembali )</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">Ok</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>