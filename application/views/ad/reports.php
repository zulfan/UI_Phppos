<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
  .box{
    width: 100%;
    position: relative;
    background: white;
    border-top: 3px solid #ffffff;
    margin-bottom: 20px;
  }
  .box1{
    width: 100%;
    position: relative;
    background: white;
    border-top: 3px solid #ffffff;
    margin-bottom: 20px;
    border-radius: 5px;
  }
</style>
<div class="row">
  <div class="col-md-6">
    <div class="box">
      <div class="box-body">
          <table class="table table-hover table-bordered">
            <tbody>
              <tr>
                <td><a href="<?= base_url('') ?>"><i class="fa fa-th"></i> Categories</a></td>
              </tr>
              <tr>
                <td><i class="fa fa-close"></i> Closeout</td>
              </tr>
              <tr>
                <td><i class="fa fa-search"></i> Custom Reports</td>
              </tr>
              <tr>
                <td><i class="fa fa-dollar"></i> Commission</td>
              </tr>
              <tr>
                <td><i class="fa fa-user"></i> Customers</td>
              </tr>
              <tr>
                <td><i class="fa fa-trash"></i> Deleted sales</td>
              </tr>
              <tr>
                <td>Discount</td>
              </tr>
              <tr>
                <td><i class="fa fa-newspaper-o"></i> Employees</td>
              </tr>
              <tr>
                <td><i class="fa fa-dollar"></i> Expenses</td>
              </tr>
              <tr>
                <td><i class="fa fa-credit-card"></i> Giftcards</td>
              </tr>
              <tr>
                <td><i class="fa fa-industry"></i> Inventory Reports</td>
              </tr>
              <tr>
                <td><i class="fa fa-hdd-o"></i> Items</td>
              </tr>
              <tr>
                <td><i class="fa fa-dollar"></i> Payments</td>
              </tr>
              <tr>
                <td><i class="fa fa-shopping-cart"></i> Provit and Loss</td>
              </tr>
              <tr>
                <td><i class="fa fa-cloud-download"></i> Receivings</td>
              </tr>
              <tr>
                <td><i class="fa fa-search"></i> Register Logs</td>
              </tr>
              <tr>
                <td><i class="fa fa-cart-plus"></i> Sales</td>
              </tr>
              <tr>
                <td><i class="fa fa-download"></i> Suppliers</td>
              </tr>
              <tr>
                <td><i class="fa fa-download"></i> Suspended Sales </td>
              </tr>
              <tr>
                <td><i class="fa fa-th"></i> Tags</td>
              </tr>
              <tr>
                <td><i class="fa fa-file"></i> Taxes</td>
              </tr>
              <tr>
                <td><i class="fa fa-line-chart"></i> Tiers</td>
              </tr>
            </tbody>
          </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="box1">
      <h3><span> << Reports: Make a selection</span></h3>
        <hr>
    </div>
  </div>
    

<?php endsection() ?>

<?php getview('layouts/layout') ?>