<form action="#" class="col-md-8">
	<div class="form-group">
		<label class="col-md-2 control-label">Golongan</label>
		<div class="col-md-5">
			<?= $this->form->text('class', null, 'class="form-control"') ?>
		</div>
	</div>	
	<div class="form-group">
		<label class="col-md-2 control-label">Keterangan</label>
		<div class="col-md-5">
			<?= $this->form->text('class', null, 'class="form-control"') ?>
		</div>
	</div>			
	<div class="form-group">
		<div class="col-md-offset-2 col-md-4">
			<button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-ok"></i> Simpan</button>
		</div>
		<div class="col-md-offset-2">
			<a href="<?= base_url('siswa/customer') ?>" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i> Batal</a>
		</div>
	</div>		
</form>