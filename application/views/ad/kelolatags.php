<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
  .box{
    height: 1290x;
    width: 100%;
  }
  .box-header{
    background-color: #d2d6de;
  }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Kelola tags</h3>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <form class="form-horizontal"> 
                        <h5><a href="#modal1" data-toggle="modal">[Add Tag]</a></h5>
                        <h5>Pets</h5>
                        <h5><ul><a href="#modal2" data-toggle="modal">[Edit]</a></ul></h5>
                        <h5><ul><a href="#modal3" data-toggle="modal">[Hapus]</a></ul></h5>
                        <h5>Snack</h5>
                        <h5><ul><a href="#modal2" data-toggle="modal">[Edit]</a></ul></h5>
                        <h5><ul><a href="#modal3" data-toggle="modal">[Hapus]</a></ul></h5>
                        
                        <h5><a href="#modal1" data-toggle="modal">[Add Tag]</a></h5>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->
<div id="modal1" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Masukkan Nama tag</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->
<div id="modal2" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Masukkan Nama tag</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-md-12">
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal HTML -->
<div id="modal3" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p>Apakah Anda yakin ingin menghapus tag ini?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary">Ok</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>



<?php endsection() ?>

<?php getview('layouts/layout') ?>