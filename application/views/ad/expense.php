<?php section('css') ?>
<link href="test.css">
<?php endsection() ?>
<style>
  .a{
    position: fixed;
    top: 20px;
    right: 5px;
  }
  .form {
    width:500px;
    margin:50px auto;
  }
  .search {
    padding:8px 15px;
    background:rgba(5, 5, 5, 0.1);
    border:0px solid #f2f6f9;
  }
  .table {
    border-collapse: collapse;
    width: 100%;
    }
  .th, .td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
    }
  .tr:hover{background-color:#f5f5f5}
  .button{
    background-color: blue;
  }
  .page-heading{
    background-color: white;
    margin-bottom: 5px;
    width: 100%;
    padding: 20px;
  }
</style>
<?php section('content') ?>
  <section>
    <div id='main-content'>
      <header class='page-heading'>
        <div>
          <div class="row">
            <div class="col-md-2">
              <form>
                <input class="search" type="text" placeholder="Cari Expenses" required>
              </form>
            </div>
            <div class="col-md-offset-6 col-md-2" align="right">
              <a href="<?= base_url('ad/newexpense')?>" class="btn btn-larg btn-primary" >Expense Baru</a>
            </div>
            <div class="col col-md-1" align="right">
              <a href="#" class="btn btn-larg btn-danger" >Hapus</a>
            </div>
          </div>
        </div>
      </header>
    </div>
  </section>
  <section>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Expense Daftar</h3>
          <span class="badge" style="background-color: #367fa9;">1</span>
      </div>
        <div class="box-body">
          <table class="table">
            <div class="col-md-12">
              <thead>
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th class="th">Id</th>
                    <th class="th">Tipe</th>
                    <th class="th">Deskripsi</th>
                    <th class="th">Kategori</th>
                    <th class="th">Tanggal</th>
                    <th class="th">Jumlah</th>
                    <th class="th">Nama Penerima</th>
                    <th class="th">Disetujui oleh</th>
                    <th class="th"></th>
                    <th class="th"></th>
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox"></td>
                    <td class="td">1</td>
                    <td class="td">Entertainment</td>
                    <td class="td">Sales Lunch</td>
                    <td class="td">Travel & Entertaintment</td>
                    <td class="td">04/14/2016</td>
                    <td class="td">$50.00</td>
                    <td class="td">$20.00</td>
                    <td class="td">Man Sales</td>
                    <td class="td">Doe, john</td>
                    <td class="td"><a href="<?= base_url('ad/')?>">edit</a></td>
                  </tr>
                </thead>
              </thead>
            </div>
          </table>
        </div>
      </div>
    </section>
<?php endsection() ?>

<?php getview('layouts/layout') ?>