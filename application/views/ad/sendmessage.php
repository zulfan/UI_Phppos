<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
  .box{
    width: 100%;
    border-color: #ffffff;
  }
  .box1{
    width: 100%;
    background-color: #FFFFFF;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header">
				<div class="box-body">
					<div class="row">
          	<div class="col-md-offset-8">
                  <a href="<?= base_url('ad/viewall')?>"><input type="submit" name="submit" value="Kotak Masuk" id="submitf" class=" submit_button btn btn-success"></a>
                  <a href="<?= base_url('ad/sendmessage')?>"><input type="submit" name="submit" value="Mengirim Pesan" id="submitf" class=" submit_button btn btn-primary"></a>
                  <a href="<?= base_url('ad/sentmessage')?>"><input type="submit" name="submit" value="Pesan yang Dikirim" id="submitf" class=" submit_button btn btn-warning"></a>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box1">
      <div class="box-header">
        <div class="box-header with-border">
          <i class="fa fa-pencil"></i>
            <h2 class="box-title">Informasi Pesan</h2>
        </div>
        <div class="box-body">
        <div class="form-group">
          <label class="col-md-3">Employees :</label>
            <div class="col-md-8">
              <div class="input-group">
                <span class="input-group-btn">
                  <button class="btn btn-primary" type="button">
                    <div class="form-group">
                      <div class="col-sm-8">
                        <input type="checkbox">
                      </div>
                    </div><span>Semua</span>
                  </button>
                </span>
                <input type='text' class="form-control" style="">
              </div>
            </div>
        </div>
        <div class="form-group">
          <label class="col-md-3">Pesan :</label>
            <div class="col-sm-8">
              <textarea class="form-control" rows="3"></textarea>
            </div>
        </div>
        <div class="col-md-offset-11">
          <input type="submit" name="submit" value="Submit" id="submitf" class=" submit_button btn btn-primary">
        </div>

          
        </div>
      </div>
    </div>
  </div>
</div>

<?php endsection() ?>

<?php getview('layouts/layout') ?>