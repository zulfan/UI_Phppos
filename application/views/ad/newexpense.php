<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
    .box{
        height: 730px;
        width: 100%;
    }
    .box-header{
    background-color: #d2d6de;
    }
    .form-group .input-group-addon {
    background: #f2f4f7; 
    }
    i{
    color: #555555;
    font-size: 20px;
    }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <i class="fa fa-pencil"></i>
          <h2 class="box-title">Expense Baru</h2>
      </div>
      <h5>&nbsp;&nbsp;&nbsp;Expense Basic Information</h5>
      <div class="box-body">
        <div class="col-md-12">
          <form class="form-horizontal"> 
            <div class="form-group">
              <label class="col-md-3">Tanggal :</label>
                <div class="col-md-8">
                  <div class="input-group date" data-date="">
                    <span class="input-group-addon bg">
                        <i class="ion ion-ios-calendar-outline"></i>
                    </span>
                    <input name="start_date" value="" id="start_date" class="form-control datepicker" type="text">
                  </div>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Jumlah :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Deskripsi :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Tipe :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Alasan :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Kategori :</label>
                <div class="col-md-8">
                  <select name="" class="form-control">
                    <option value="0">Pilih kategori</option>
                    <option value="1">Cal</option>
                    <option value="2">Categ1</option>
                    <option value="3">Clothing</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Nama Penerima :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="John Doe">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Disetujui oleh :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="John Doe">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Catatan Expense :</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
            </div>
            <div class="col-md-offset-11">
              <input type="submit" name="submit" value="Submit" id="submitf" class=" submit_button btn btn-primary">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
           

<?php endsection() ?>

<?php getview('layouts/layout') ?>