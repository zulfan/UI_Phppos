<?php section('css') ?>
<link href="test.css">
<?php endsection() ?>
<style>
  .box{
    height: 110px;
    width: 100%;
  }
  .list a{
    height: 110px;
    width: 100%;
  }
</style>
<?php section('content') ?>
<div class="row">

        <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Belum dibayar&nbsp;</h2><small>(dalam IDR)</small>
            </div>
            <div class="box-body">
                <h3>Rp 0,00</h3>
            </div><!-- /.box-body --><!-- /.box-footer-->
        </div><!-- /.box -->
</div>
<div class="row">

        <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Belum dibayar&nbsp;</h2><small>(dalam IDR)</small>
            </div>
            <div class="box-body">
                <h3>Rp 0,00</h3>
            </div><!-- /.box-body --><!-- /.box-footer-->
        </div><!-- /.box -->
</div>
<div class="row">

        <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Belum dibayar&nbsp;</h2><small>(dalam IDR)</small>
            </div>
            <div class="box-body">
                <h3>Rp 0,00</h3>
            </div><!-- /.box-body --><!-- /.box-footer-->
        </div><!-- /.box -->
</div>
<div class="row">

        <div class="box">
            <div class="box-header with-border">
              <h2 class="box-title">Belum dibayar&nbsp;</h2><small>(dalam IDR)</small>
            </div>
            <div class="box-body">
                <h3>Rp 0,00</h3>
            </div><!-- /.box-body --><!-- /.box-footer-->
        </div><!-- /.box -->
</div>
<div class="" align="center">
<label>Welcome to PHP Point Of Sale, choose a common task below to get started!</label><p>
<div class="col-md-6">
  <div class="list-group">
    <a class="list-group-item" href="#"> <i class="ion-clock"></i> Today's closeout report</a>
  </div>
</div>
<div class="col-lg-6">
  <div class="list-group">
    <a class="list-group-item" href="#"> <i class="ion-stats-bars"></i> Today's detailed sales report</a>
  </div>
</div>
<div class="col-lg-6">
  <div class="list-group">
    <a class="list-group-item" href="#"> <i class="ion-clipboard"></i> Today's summary items report</a>
  </div>
</div>
<div class="col-lg-6">
  <div class="list-group">
    <a class="list-group-item" href="#"> <i class="icon ti-cloud-down"></i> Start a New Receiving</a>
  </div>
</div>
<div class="col-lg-6">
  <div class="list-group">
    <a class="list-group-item" href="#"> <i class="icon ti-shopping-cart"></i> Start a New Sale</a>
  </div>
</div>
<div class="row ">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-body">
        <div class="panel-heading">
        <h4 class="text-center">Sales Information</h4>  
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs piluku-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#month" data-type="monthly" aria-controls="month" role="tab">Month</a></li>
            <li role="presentation"><a href="#week" data-type="weekly" aria-controls="week" role="tab">Week</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content piluku-tab-content">
          <div role="tabpanel" class="tab-pane active" id="month">
            <div class="chart">
            <canvas id="charts" width="539" height="134" style="width: 539px; height: 134px;"></canvas>   
            </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="week">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<label>Please visit our website to learn the latest information about the project. You are using PHP Point Of Sale Version 15.0 Built on 04/06/2016 07:05 pm</label>
<?php endsection() ?>

<?php getview('layouts/layout') ?>
