<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
  .box{
    height: 600px;
    width: 100%;
  }
  .box1{
    background-color: #ffffff;
    height: 580px;
    width: 100%;
  }
  .box-header{
    background-color: #d2d6de;
  }
  .fa{
    color: #555564;
  }
  .form-group .input-group-addon {
   background: #489ee7; 
  }
  i{
    color: #ffffff;
    font-size: 20px;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <i class="fa fa-pencil"></i>
          <h2 class="box-title">Informasi Barang&nbsp;</h2>
      </div>
      <div class="box-body">
        <div class="col-md-12">
          <form class="form-horizontal"> 
            <div class="form-group">
              <label class="col-md-3">Nama Barang :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Kategori Barang :</label>
                <div class="col-md-8">
                  <select name="" class="form-control">
                    <option value="0">Pilih kategori</option>
                    <option value="1">Bieren</option>
                    <option value="2">fuel</option>
                    <option value="3">Travel & Entertaintment</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Ukuran :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Pemasok :</label>
                <div class="col-md-8">
                  <select name="" class="form-control">
                    <option value="0">Pilih Pemasok</option>
                    <option value="1">None</option>
                    <option value="2">AamarTech</option>
                    <option value="3">Dawn Plastics</option>
                  </select>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Deskripsi :</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Pilih Avatar </label>
                <div class="col-md-8">
                  <div class="input-group">
                    <input type='text' class="form-control">
                      <span class="input-group-btn">
                        <button class="btn btn-primary" type="button"><span>Pilih File</span></button>
                      </span>
                  </div>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3"></label>
                <div class="col-md-8">
                  <img src="<?= base_url('public/img/avatar (1).png') ?>">
                </div>
            </div>
          </form>
        </div>
      </div>   
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box1">
      <div class="box-header with-border">
        <i class="fa fa-pencil"></i>
          <h4 class="box-title">Harga dan Inventaris</h4>
      </div>
      <div class="box-body">
          <div class="col-md-12">
            <form class="form-horizontal"> 
              <div class="form-group">
                <label class="col-md-3">Harga (tanpa pajak) :</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control">
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">Ubah harga selama sale :</label>
                  <div class="col-md-8">
                    <input type="checkbox">
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">Margin :</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" placeholder="Masukkan persentase margin untuk menghitung harga jual">
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">Harga Penjualan: </label>
                  <div class="col-md-8">
                    <input type="text" class="form-control">
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">Harga Promo :</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control">
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">Promo dimulai tanggal :</label>
                  <div class="col-md-8">
                    <div class="input-group date" data-date="">
                      <span class="input-group-addon bg">
                          <i class="ion ion-ios-calendar-outline"></i>
                      </span>
                      <input name="start_date" value="" id="start_date" class="form-control datepicker" type="text">
                    </div>
                  </div>
              </div>
              <div class="form-group">
                <label class="col-md-3">Promo berakhir tanggal :</label>
                  <div class="col-md-8">
                    <div class="input-group date" data-date="">
                      <span class="input-group-addon bg">
                          <i class="ion ion-ios-calendar-outline"></i>
                      </span>
                      <input name="start_date" value="" id="start_date" class="form-control datepicker" type="text">
                    </div>
                  </div>
              </div>
            </form>
          </div> 
    </div>
  </div>
</div>

<?php endsection() ?>

<?php getview('layouts/layout') ?>