<?php section('css') ?>
<link href="test.css">
<?php endsection() ?>
<style>
  .a{
    position: fixed;
    top: 20px;
    right: 5px;
  }
  .form {
    width:500px;
    margin:50px auto;
  }
  .search {
    padding:8px 15px;
    background:rgba(5, 5, 5, 0.1);
    border:0px solid #f2f6f9;
  }
  .table {
    border-collapse: collapse;
    width: 100%;
    }
  .th, .td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
    }
  .tr:hover{background-color:#f5f5f5}
  .button{
    background-color: blue;
  }
  .page-heading{
    background-color: white;
    margin-bottom: 5px;
    width: 100%;
    padding: 20px;
  }
</style>
<?php section('content') ?>
  <section>
    <div id='main-content'>
      <header class='page-heading'>
        <div>
          <div class="row">
            <div class="col-md-2">
              <form>
                <input class="search" type="text" placeholder="Cari Suppliers" required>
              </form>
            </div>
            <div class="col-md-offset-1 col-md-7" align="right">
              <a href="<?= base_url('ad/newsupplier')?>" class="btn btn-larg btn-primary" >Supplier Baru</a>
            </div>
            <div class="dropdown col-md-2">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="padding-right : 23px; padding-left: 23px;">.  .  .</button>
              <ul class="dropdown-menu">
                <li><a href="<?= base_url('ad/excelimpor')?>">Excel Impor</a></li>
                <li><a href="#">Excel Expor</a></li>
                <li><a href="#myModal" data-toggle="modal">Pembersihan supplier lama</a></li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </div>
  </section>
  <section>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Suppliers</h3>
          <span class="badge" style="background-color: #367fa9;">2</span>
      </div>
      <div class="box-body">
        <table class="table">
          <div class="col-md-12">
            <thead>
              <thead>
                <tr>
                  <th><input type="checkbox" aria-label="..." style="border-top: 5px; border-left: 5px; border-right: 5px;"></th>
                  <th class="th">ID Supplier</th>
                  <th class="th">Nama Perusahaan</th>
                  <th class="th">NAma Belakang</th>
                  <th class="th">Nama Depan</th>
                  <th class="th">E-Mail</th>
                  <th class="th">Nomor Tlp.</th>
                  <th class="th"></th> 
                </tr>
                <tr class="tr">
                  <td class="td"><input type="checkbox" aria-label="..."></td>
                  <td class="td">12</td>
                  <td class="td">Byte</td>
                  <td class="td"></td>
                  <td class="td">Eddy</td>
                  <td class="td"></td>
                  <td class="td"></td>
                  <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35"></td>
                </tr>
                <tr class="tr">
                  <td class="td"><input type="checkbox" aria-label="..."></td>
                  <td class="td">6</td>
                  <td class="td">Regie Sellers Inc.</td>
                  <td class="td">Sellers</td>
                  <td class="td">Regie</td>
                  <td class="td"></td>
                  <td class="td"></td>
                  <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35"></td>
                </tr>
              </thead>
            </thead>
          </div>
        </table>
      </div>
    </div>
  </section>
  <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Konfirmasi</h4>
          </div>
          <div class="modal-body">
            <p>Apakah Anda yakin Anda ingin menghapus Supplier ? ( Ini akan menghapus nomor rekening dari Supplier dihapus )</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">Ok</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
<?php endsection() ?>
<?php getview('layouts/layout') ?>