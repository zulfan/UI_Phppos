<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
  .box{
    height: 1290x;
    width: 100%;
  }
  .box-header{
    background-color: #d2d6de;
  }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h2 class="box-title"><small>Data impor massal dari lembar excel</small></h2>
            </div>
            <div class="box-body">
                <div class="col-md-12">
                    <form class="form-horizontal"> 
                        <h3>Langkah 1</h3>
                        <h5>Download salah satu file di bawah ini dan menambahkan / memperbarui pelanggan Anda .</h5>
                            <input type="submit" name="submit" value="Download Template excel untuk pelanggan baru" id="submitf" class=" submit_button btn btn-success">
                            <h6>OR</h6>
                            <input type="submit" name="submit" value="Download Template excel untuk pelanggan yang sudah ada" id="submitf" class=" submit_button btn btn-success">
                        <h3>Langkah 2</h3>
                        <h5>Upload file dari langkah 1 untuk menyelesaikan penambahan pelanggan Anda / update</h5>
                        <div class="form-group">
                            <label class="col-md-3">File path </label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type='text' class="form-control">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="button"><span>Pilih File</span></button>
                                            </span>
                                    </div>
                                </div>
                        </div>
                        <div class="col-md-offset-10">
                            <input type="submit" name="submit" value="Submit" id="submitf" class=" submit_button btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



<?php endsection() ?>

<?php getview('layouts/layout') ?>