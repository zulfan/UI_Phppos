<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
  .box{
    height: 1200px;
    width: 100%;
  }
  .box-header{
    background-color: #d2d6de;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <i class="fa fa-pencil"></i>
          <h2 class="box-title">Informasi Pelanggan&nbsp;</h2>
      </div>
      <div class="box-body">
        <div class="col-md-12">
          <form class="form-horizontal"> 
            <div class="form-group">
              <label class="col-md-3">Nama Depan :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Nama Belakang :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">E-Mail :</label>
                <div class="col-md-8">
                  <input type="email" class="form-control" value="">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Nomor Telp. :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Pilih Avatar </label>
                <div class="col-md-8">
                  <div class="input-group">
                    <input type='text' class="form-control">
                      <span class="input-group-btn">
                        <button class="btn btn-primary" type="button"><span>Pilih File</span></button>
                      </span>
                  </div>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3"></label>
                <div class="col-md-8">
                  <img src="<?= base_url('public/img/avatar (1).png') ?>">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Alamat 1 :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Alamat 2 :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Kota :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Negara Bagian / Provinsi :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Zip :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Negara :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Komentar :</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Saldo Toko Akun :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="0.00">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Batas Kredit :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Poin :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="0.00">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Nama Perusahaan :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Akun # :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="col-md-offset-11">
              <input type="submit" name="submit" value="Submit" id="submitf" class=" submit_button btn btn-primary">
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<?php endsection() ?>

<?php getview('layouts/layout') ?>