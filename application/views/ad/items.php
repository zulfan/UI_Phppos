<?php section('css') ?>
<link href="test.css">
<?php endsection() ?>
<style>
  .a{
    position: fixed;
    top: 20px;
    right: 5px;
  }
  .form {
    width:500px;
    margin:50px auto;
  }
  .search {
    padding:8px 15px;
    background:rgba(5, 5, 5, 0.1);
    border:0px solid #f2f6f9;
  }
  .table {
    border-collapse: collapse;
    width: 100%;
    }
  .th, .td {
    padding: 8px;
    text-align: left;
    border-bottom: 1px solid #ddd;
    }
  .tr:hover{background-color:#f5f5f5}
  .button{
    background-color: blue;
  }
  .page-heading{
    background-color: white;
    margin-bottom: 5px;
    width: 100%;
    padding: 20px;
  }
</style>
<?php section('content') ?>
  <section>
    <div id='main-content'>
      <header class='page-heading'>
        <div>
          <div class="row">
            <div class="col-md-2">
              <form>
                <input class="search" type="text" placeholder="Cari barang" required>
              </form>
            </div>
            <form action="" class="col-md-2" >
              <select name="fields" class="form-control" id="fields">
                <div class="col col-md-2">
                  <option value="all" selected="selected">All</option>
                  <option value="phppos_items.item_id">Id Barang</option>
                  <option value="phppos_items.product_id">Produk ID</option>
                  <option value="phppos_items.name">Nama Barang</option>
                  <option value="phppos_items.description">Deskripsi</option>
                  <option value="phppos_items.size">Ukuran</option>
                  <option value="phppos_items.cost_price">Harga</option>
                  <option value="phppos_items.unit_price">Harga Penjualan</option>
                  <option value="phppos_items.promo_price">Harga Promo</option>
                  <option value="phppos_location_items.quantity">Kuantitas</option>
                  <option value="phppos_suppliers.company_name">Supplier</option>
                  <option value="phppos_tags.name">Tag</option>
                </div>
              </select>
            </form>
            <form action="" class="col-md-2" >
              <select name="fields" class="form-control" id="fields">
                <div class="col col-md-1">
                  <option value="all" selected="selected">All</option>
                  <option value="phppos_items.item_id">Bieren</option>
                  <option value="phppos_items.item_number">fuel</option>
                  <option value="phppos_items.product_id">LEGGING</option>
                </div>
              </select>
            </form>
            <div class="col col-md-1">
              <a href="#" class="btn btn-larg btn-primary" >Cari</a>
            </div>
            <div class="col-md-offset-1 col-md-2" align="right">
              <a href="<?= base_url('ad/newitem')?>" class="btn btn-larg btn-primary" >Barang baru</a>
            </div>
            <div class="dropdown col-md-2">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" style="padding-right : 23px; padding-left: 23px;">.  .  .</button>
                <ul class="dropdown-menu">
                  <li><a href="<?= base_url('ad/kelolatags')?>">Kelola Tags</a></li>
                  <li><a href="<?= base_url('ad/excelimpor')?>">Excel Impor</a></li>
                  <li><a href="#">Excel Expor</a></li>
                  <li><a href="#myModal" data-toggle="modal">Pembersihan pelanggan lama</a></li>
                </ul>
            </div>
          </div>
        </div>
      </header>
    </div>
  </section>
  <section>
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Barang</h3>
          <span class="badge" style="background-color: #367fa9;">6</span>
      </div>
        <div class="box-body">
          <table class="table">
            <div class="col-md-12">
              <thead>
                <thead>
                  <tr>
                    <th><input type="checkbox"></th>
                    <th class="th">Barang</th>
                    <th class="th">Id</th>
                    <th class="th">Nama</th>
                    <th class="th">Kategori</th>
                    <th class="th">Ukuran</th>
                    <th class="th">Harga</th>
                    <th class="th">Harga Penjualan</th>
                    <th class="th">Kuantitas</th>
                    <th class="th">Inventaris</th>
                    <th class="th">Clone</th>
                    <th class="th">Edit</th>
                    <th class="th"></th>
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox"></td>
                    <td class="td">3</td>
                    <td class="td"></td>
                    <td class="td"><a href="">Store Account Payment</a></td>
                    <td class="td"></td>
                    <td class="td"></td>
                    <td class="td">$0.00</td>
                    <td class="td">$0.00</td>
                    <td class="td">Not set</td>
                    <td class="td"></td>
                    <td class="td"><a href="">Clone</a></td>
                    <td class="td"><a href="">Edit</a></td>
                    <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35"></td>
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox"></td>
                    <td class="td">4</td>
                    <td class="td"></td>
                    <td class="td"><a href="">Table</a></td>
                    <td class="td">Bieren</td>
                    <td class="td"></td>
                    <td class="td">$20.50</td>
                    <td class="td">$23.50</td>
                    <td class="td">Not set</td>
                    <td class="td"></td>
                    <td class="td"><a href="">Clone</a></td>
                    <td class="td"><a href="">Edit</a></td>
                    <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35"></td>
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox"></td>
                    <td class="td">1</td>
                    <td class="td"></td>
                    <td class="td"><a href="">Quadrum 12</a></td>
                    <td class="td">Bieren</td>
                    <td class="td">Size Large</td>
                    <td class="td">$1.00</td>
                    <td class="td">$1.60</td>
                    <td class="td">23</td>
                    <td class="td"><a href="">Inventory</a></td>
                    <td class="td"><a href="">Clone</a></td>
                    <td class="td"><a href="">Edit</a></td>
                    <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35"></td>
                  </tr>
                  <tr class="tr">
                    <td class="td"><input type="checkbox"></td>
                    <td class="td">2</td>
                    <td class="td"></td>
                    <td class="td"><a href="">A Day in life of teacher</a></td>
                    <td class="td">Bieren</td>
                    <td class="td"></td>
                    <td class="td">$2.00</td>
                    <td class="td">$4.00</td>
                    <td class="td">28</td>
                    <td class="td"><a href="">Inventory</a></td>
                    <td class="td"><a href="">Clone</td>
                    <td class="td"><a href="">Edit</td>
                    <td class="td"><img src="<?= base_url('public/img/picture1.png') ?>" width="35"></td>
                  </tr>
                </thead>
              </thead>
            </div>
          </table>
    </div>
  </section>
   <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Konfirmasi</h4>
          </div>
          <div class="modal-body">
            <p>Apakah Anda yakin Anda ingin membersihkan semua item yang dihapus ? ( Ini akan menghapus nomor item dari item yang dihapus sehingga mereka dapat digunakan kembali )</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary">Ok</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          </div>
        </div>
      </div>
    </div>
<?php endsection() ?>

<?php getview('layouts/layout') ?>