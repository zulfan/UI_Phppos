<?php section('content') ?>
<!-- Content Header (Page header) -->
<!-- Main content -->
<style>
  .box{
    height: 1170px;
    width: 100%;
  }
  .box1{
    background-color: #ffffff;
    height: 580px;
    width: 100%;
  }
  .box-header{
    background-color: #f2f4f7;
  }
  .fa{
    color: #555564;
  }
  .form-group .input-group-addon {
   background: #489ee7; 
  }
  i{
    color: #ffffff;
    font-size: 20px;
  }
  h3{
    font-family: georgia;
    color: #99d8ff;
  }
</style>
<div class="row">
  <div class="col-md-12">
    <div class="box">
      <div class="box-header with-border">
        <i class="fa fa-pencil"></i>
          <h2 class="box-title">Edit Profil&nbsp;</h2>
      </div>
      <div class="box-body">
        <div class="col-md-12">
          <form class="form-horizontal"> 
            <div class="form-group">
              <label class="col-md-3">Nama Depan :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="John">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Nama Belakang :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="Doe">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">E-Mail :</label>
                <div class="col-md-8">
                  <input type="email" class="form-control" value="no-reply@phppointofsale.com">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Nomor Telp. :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="555-555-5555">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Pilih Avatar </label>
                <div class="col-md-8">
                  <div class="input-group">
                    <input type='text' class="form-control">
                      <div class="input-group">
                        <input type='file' id="exampleInputFile">
                      </div>
                  </div>
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3"></label>
                <div class="col-md-8">
                  <img src="<?= base_url('public/img/avatar (1).png') ?>">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Alamat 1 :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="Alamat 1">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Alamat 2 :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Kota :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Negara Bagian / Provinsi :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Zip :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Negara :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Komentar :</label>
                <div class="col-md-8">
                  <textarea class="form-control" rows="3"></textarea>
                </div>
            </div>
            <h3>Info Login Karyawan</h3>
            <hr>
            <div class="form-group">
              <label class="col-md-3">Nama Pengguna :</label>
                <div class="col-md-8">
                  <input type="text" class="form-control" value="admin">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Password :</label>
                <div class="col-md-8">
                  <input type="password" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Masukkan ulang Password :</label>
                <div class="col-md-8">
                  <input type="password" class="form-control">
                </div>
            </div>
            <div class="form-group">
              <label class="col-md-3">Bahasa :</label>
                <div class="col-md-8">
                  <select name="bahasa" class="form-control">
                    <option value="english" selected="selected">English</option>
                    <option value="indonesia">Indonesia</option>
                    <option value="spanish">Español</option>
                    <option value="french">Fançais</option>
                    <option value="italian">Italiano</option>
                    <option value="german">Deutsch</option>
                    <option value="dutch">Nederlands</option>
                    <option value="portugues">Portugues</option>
                    <option value="arabic">العَرَبِيةُ‎‎</option>
                    <option value="khmer">Khmer</option>
                  </select>
                </div>
            </div>
            <div class="col-md-offset-5">
              <input type="submit" name="submitf" value="Submit" id="submitf" class="btn btn-primary" style="padding-right: 45px; padding-left: 45px;">
          </form>
        </div>
      </div>   
    </div>
  </div>
</div>

<?php endsection() ?>

<?php getview('layouts/layout') ?>